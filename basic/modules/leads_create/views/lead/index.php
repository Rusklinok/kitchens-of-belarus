<?php

use app\modules\leads_create\models\Lead;
use yii\helpers\Html;
use yii\grid\GridView;


/** @var yii\web\View $this */
/** @var app\modules\leads_create\models\LeadSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Лиды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            'id',
            'name',
            'phone',
            'domain',
//            'form_name',
            'page_name',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{delete}'],
        ],
    ]); ?>


</div>
