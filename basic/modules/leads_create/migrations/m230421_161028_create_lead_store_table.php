<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lead_store}}`.
 */
class m230421_161028_create_lead_store_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lead_store}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45),
            'phone' => $this->string(20),
            'domain' => $this->string(45),
            'sub_domain' => $this->string(45),
            'form_name' => $this->string(45),
            'page_name' => $this->string(255),
            'utm_source' => $this->string(45),
            'utm_medium' => $this->string(45),
            'utm_campaign' => $this->string(45),
            'utm_content' => $this->string(45),
            'utm_term' => $this->string(45),
            'ym_uid' => $this->string(45),
            'ct_session_id' => $this->string(45),
            'created_at' => $this->integer(),
            'client_id' => $this->string(45),
            'email' => $this->string(45),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lead_store}}');
    }
}
