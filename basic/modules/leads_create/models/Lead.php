<?php

namespace app\modules\leads_create\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "lead".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $domain
 * @property string|null $sub_domain
 * @property string|null $form_name
 * @property string|null $page_name
 * @property string|null $utm_source
 * @property string|null $utm_medium
 * @property string|null $utm_campaign
 * @property string|null $utm_content
 * @property string|null $utm_term
 * @property string|null $ym_uid
 * @property string|null $ct_session_id
 * @property string|null $email
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lead_store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'domain', 'sub_domain', 'client_id', 'form_name', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_content',
                'utm_term', 'ym_uid', 'ct_session_id', 'email'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 20],
            [['client_id'], 'required'],
            [['page_name'], 'string', 'max' => 255],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
//                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'domain' => 'Домен',
            'sub_domain' => 'Поддомен',
            'form_name' => 'Форма',
            'page_name' => 'Страница',
            'utm_source' => 'Utm Source',
            'utm_medium' => 'Utm Medium',
            'utm_campaign' => 'Utm Campaign',
            'utm_content' => 'Utm Content',
            'utm_term' => 'Utm Term',
            'ym_uid' => 'Ym Uid',
            'ct_session_id' => 'Ct Session ID',
            'client_id' => 'ID клиента',
        ];
    }
}
