<?php

namespace app\modules\leads_create\controllers;

use Yii;
use app\modules\leads_create\models\Lead;

use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


class LeadAController extends ActiveController
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => ['*'],
                    'Access-Control-Allow-Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Allow-Headers' => ['content-type'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],

        ]);
        /*$behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
        */
    }

    public $modelClass = 'app\modules\lead_create\models\Lead';


    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['index'], $actions['create'], $actions['view'], $actions['update'], $actions['options']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new Lead();
        if ($model->load(Yii::$app->request->post(), '')) {

            //$model->setScenario("save");
            if ($model->save()) {
                return ['status' => 'ok', "saved_id = ".$model->id];
            }
            else
                return ['status' => 'error_validation', "errors" => $model->getErrors()];
        }
        else
            return ['status' => 'error_no_params'];
    }

}
