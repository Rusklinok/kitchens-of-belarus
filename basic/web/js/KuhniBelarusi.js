const KB_SERVER_URL = 'https://workingcrmgca.ru/leads-create/lead-a/create';

class KuhniBelarusi {

    constructor(client_id) {
        this.client_id = client_id;
        this.utm_params = ["utm_source", "utm_medium", "utm_campaign", "utm_content", "utm_term"];

        // Записываем utm в куки, чтобы не потерять
        let urlParams = new URLSearchParams(window.location.search);
        for (let i = 0; i < this.utm_params.length; i++) {
            let param_name = this.utm_params[i];
            let p = urlParams.get(param_name);
            if(p)
                document.cookie = param_name + "=" + p;
        }
    }

    send(data) {
        data.client_id = this.client_id;
        data.domain = window.location.hostname;
        data.ym_uid = this.getCookie("_ym_uid");
        data.ct_session_id = this.getCookie("_ct_session_id");
        data.page_name = document.location.pathname;

        for (let i = 0; i < this.utm_params.length; i++) {
            let param_name = this.utm_params[i];
            data[param_name] = this.getCookie(param_name);
        }

        $.post( KB_SERVER_URL , data,
            function (res) {
                console.log(res);
            });
    }

    getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }

}