# Leads-create



## Описание модуля:

Модуль позволяет сохранять по API лиды и просматривать их в Web.

## API-интерфейс:

##### Метод - POST  
##### Урл - /leads_create/lead-a/create

##### Параметры:
| Имя  | тип   | Описание |
| ------- | -------- | -------- |
| client_id *   | string(45)  | ID партнера. Обязательный.
| phone   | string(20)  | Телефон клиента
| name   | string(45)  | Имя клиента
| domain   | string(45)  | Домен
| sub_domain   | string(45)  | Поддомен
| form_name   | string(45)  | Название формы заявки
| page_name   | string(255)  | Урл страницы заявки
| utm_source   | string(45)  | UTM-метка
| utm_medium   | string(45)  | UTM-метка
| utm_campaign   | string(45)  | UTM-метка
| utm_content   | string(45)  | UTM-метка
| utm_term   | string(45)  | UTM-метка
| ym_uid   | string(45)  | ClientID в Яндекс.Метрике
| ct_session_id   | string(45)  | ct_session_id

Формат ответа - json

Варианты ответа:  
```
{"status":"ok","0":"saved_id = 6"}  
{"status":"error_validation","errors":{"client_id":["Необходимо заполнить «ID клиента»."]}} 
{"status":"error_no_params"}
```
##  Руководство пользователя:

Для установки на сайты партнеры, в шапку сайта необходимо добавить код

```
<script type="text/javascript" src="https://dev-frontend.tipscrm.ru/leads_create/lead/load-js"></script>
<script>
    var kb = new KuhniBelarusi("test_id_client_str");
</script>
```
где "test_id_client_str"  - Уникальный ID партнера


В момент отправки заявки, добавить код 

```
kb.send(data);
```
Параметры data

| Имя  | тип   | Описание |
| ------- | -------- | -------- |
| phone   | string(20)  | Телефон клиента
| name   | string(45)  | Имя клиента
| form_name   | string(45)  | Название формы заявки

